var chart; // global
/**
 * Request data from the server, add it to the graph and set a timeout to request again
 */
Highcharts.setOptions({
    time: {
        timezone: 'Europe/Amsterdam'
    }
});
function requestData() {
	var targetUrl = '/showValues';
	if(brewSessionId) {
		targetUrl = '/showValues/'+brewSessionId
	} 
	
	$.ajax({
		url: targetUrl, 
		success: function(data) {
			var temp_json = new Array();  
			var grav_json = new Array();
			
            for (i = 0; i < data.length; i++){

            	temp_json.push([data[i].dateTimeAsMs, data[i].temperature]);
            	grav_json.push([data[i].dateTimeAsMs, data[i].gravity]);
            }
            
        	chart = new Highcharts.Chart({
        		chart: {
        			renderTo: 'graph',
        			defaultSeriesType: 'spline',
        		},
        		title: {
        			text: 'Ispindel'
        		},
        		xAxis: {
        			 type: 'datetime',
                     ordinal: true
//    		      labels: {
//    		        formatter: function() {
//    		          var year = this.value.substring(0, 4);
//    		          var month = this.value.substring(5, 7);
//    		          var day = this.value.substring(8, 10);
//    		          var time = this.value.substring(11, 19);
//   		          // return `${year}/${month}/${day} ${time}`;
//    		          return `${time}`;
//    		        }
//    		      }
        		},
        		yAxis: [{ // Primary yAxis
        	        labels: {
        	            format: '{value}°C',
        	            style: {
        	                color: Highcharts.getOptions().colors[2]
        	            }
        	        },
        	        title: {
        	            text: 'Temperature',
        	            style: {
        	                color: Highcharts.getOptions().colors[2]
        	            }
        	        },
        	        opposite: true
        		},{ // Secondary yAxis
        	        gridLineWidth: 0,
        	        title: {
        	            text: 'Gravity',
        	            style: {
        	                color: Highcharts.getOptions().colors[0]
        	            }
        	        },
        	        labels: {
        	            format: '{value} sg',
        	            style: {
        	                color: Highcharts.getOptions().colors[0]
        	            }
        	        }
        		}],tooltip: {
        	        shared: true
        	    },
        	    legend: {
        	        layout: 'vertical',
        	        align: 'left',
        	        x: 80,
        	        verticalAlign: 'top',
        	        y: 55,
        	        floating: true,
        	        backgroundColor:
        	            Highcharts.defaultOptions.legend.backgroundColor || // theme
        	            'rgba(255,255,255,0.25)'
        	    },
        		series: [{
        			name: 'Temperature',
        			data: temp_json,
        			yAxis: 0,
        	        tooltip: {
        	            valueSuffix: ' °C'
        	        }        			
        		},{
        			name: 'Gravity',
        			data: grav_json,
        			yAxis: 1
        		}
        		]/*,
        	    responsive: {
        	        rules: [{
        	            condition: {
        	                maxWidth: 500
        	            },
        	            chartOptions: {
        	                legend: {
        	                    floating: false,
        	                    layout: 'horizontal',
        	                    align: 'center',
        	                    verticalAlign: 'bottom',
        	                    x: 0,
        	                    y: 0
        	                },
        	                yAxis: [{
        	                    labels: {
        	                        align: 'right',
        	                        x: 0,
        	                        y: -6
        	                    },
        	                    showLastLabel: false
        	                }, {
        	                    labels: {
        	                        align: 'left',
        	                        x: 0,
        	                        y: -6
        	                    },
        	                    showLastLabel: false
        	                }, {
        	                    visible: false
        	                }]
        	            }
        	        }]
        	    } */       		
        	});		
		},
	});
}

$(document).ready(function() {
	requestData();
});