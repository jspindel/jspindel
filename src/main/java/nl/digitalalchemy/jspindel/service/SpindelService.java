package nl.digitalalchemy.jspindel.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import nl.digitalalchemy.jspindel.dao.SpindelRepository;
import nl.digitalalchemy.jspindel.dto.BrewSessionDto;
import nl.digitalalchemy.jspindel.dto.SpindelDto;
import nl.digitalalchemy.jspindel.entity.SpindelEntity;

@Service
public class SpindelService {
	
	@Autowired
	BrewSessionService brewSessionService;
	
	@Autowired
	SpindelRepository spindelRepository;

	public List<SpindelDto> retrieveListOfSpindels() {

		List<SpindelDto> returnList = StreamSupport.stream(spindelRepository.findAll().spliterator(),false)
				.map(entity -> new SpindelDto(entity.getName(),entity.getId()))
				.collect(Collectors.toList());
		
		List<BrewSessionDto> openSessions = brewSessionService.retrieveOpenSessions();
		
		if(!CollectionUtils.isEmpty(openSessions)) {
			//TODO can we make a nice stream for this?
			for(BrewSessionDto session:openSessions) {
				for(SpindelDto spindel:returnList) {
					if(session.getSpindelName().equals(spindel.getName())) {
						spindel.setActiveBrewSession(true);
					}
				}
			}
		}
		
		return returnList;
	}
	
	public SpindelEntity addNewSpindel(String spindelName) {
		Optional<SpindelEntity> spindel = spindelRepository.findByName(spindelName);
		SpindelEntity newSpindel = null;
		
		if(!spindel.isPresent()) {
			newSpindel = new SpindelEntity(spindelName);
			spindelRepository.save(newSpindel);
		} else {
		 //TODO throw already present exception ?
		}
		
		return newSpindel;
	}
	
	public SpindelEntity findSpindelByName(String spindelName) {
		Optional<SpindelEntity> spindel = spindelRepository.findByName(spindelName);
		if(spindel.isPresent()) {
			return spindel.get();
		} else {
			return null;
		}
	}

	public SpindelEntity findSpindelById(Long spindelId) {
		Optional<SpindelEntity> spindel = spindelRepository.findById(spindelId);
		if(spindel.isPresent()) {
			return spindel.get();
		} else {
			return null;
		}
	}
	
	public SpindelEntity findOrCreateSpindelByName(String spindelName) {
		SpindelEntity spindel = this.findSpindelByName(spindelName);
		
		if(spindel == null) {
			spindel = this.addNewSpindel(spindelName);
		}

		return spindel;
		
	}
}
