package nl.digitalalchemy.jspindel.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import nl.digitalalchemy.jspindel.dao.ReadingRepository;
import nl.digitalalchemy.jspindel.dto.IspindelPostMessage;
import nl.digitalalchemy.jspindel.dto.ReadingDto;
import nl.digitalalchemy.jspindel.entity.BrewSessionEntity;
import nl.digitalalchemy.jspindel.entity.ReadingEntity;
import nl.digitalalchemy.jspindel.entity.SpindelEntity;

@Service
public class ReadingService {

	@Autowired
	ReadingRepository readingRepository;

	@Autowired
	BrewSessionService brewSessionService;
	
	@Autowired
	SpindelService spindelService;
	
	public List<ReadingDto> retrieveLastReadings() {
		
		List<BrewSessionEntity> openSessions = brewSessionService.retrieveOpenBrewSessionEntities();
		
		if(!CollectionUtils.isEmpty(openSessions)) {
			return retrieveReadingsForBrewSessionEntity( openSessions.get(0));
		}
		
		return null;//TODO is this what we want ?
		
		//TODO .. what readings do we actually want ?
//		return readingRepository.findFirst20ByOrderByIdAsc().stream()
//				.map(reading -> convertReadingEntity(reading))
//		        .collect(Collectors.toList());
	}
	
	private  List<ReadingDto> retrieveReadingsForBrewSessionEntity(BrewSessionEntity brewSessionEntity) {

		return readingRepository.findBySpindel_IdAndDateTimeBetweenOrderByIdAsc(brewSessionEntity.getSpindels().get(0).getId(), brewSessionEntity.getStartDateTime(), brewSessionEntity.getEndDateTime()!=null ? brewSessionEntity.getEndDateTime(): LocalDateTime.now()).stream()
				.map(reading -> convertReadingEntity(reading))
		        .collect(Collectors.toList());
	}
	
	public List<ReadingDto> retrieveReadingsForBrewSession(Long brewSessionId) {
		
		List<ReadingDto> dtoList = null;
		
		BrewSessionEntity brewSession = brewSessionService.retrieveBrewSessionEntityById(brewSessionId);
		
		if(brewSession != null) {
			dtoList = retrieveReadingsForBrewSessionEntity(brewSession);
		}
		
		return dtoList;
	}
	
	public ReadingDto findLatestReading() {
		
		ReadingDto readingData = null;
		
		ReadingEntity reading = readingRepository.findFirstByOrderByIdDesc();
		
		if(reading != null) {
			readingData = this.convertReadingEntity(reading);
		}
		
		return readingData;
	}
	
	private ReadingDto convertReadingEntity(ReadingEntity reading) {
		//TODO https://www.baeldung.com/entity-to-and-from-dto-for-a-java-spring-application
		BigDecimal thousant = new BigDecimal(1000);

		ReadingDto readingData = new ReadingDto();
		
		readingData.setAngle(reading.getAngle());
		readingData.setBattery(reading.getBattery());
		readingData.setDateTime(reading.getDateTime());
		readingData.setGravity(reading.getGravity().multiply(thousant));
		readingData.setSpindleName(reading.getSpindel().getName());
		readingData.setTemperature(reading.getTemperature());
		
		return readingData;
	}

	public boolean deleteReadingsReturnSuccess(LocalDateTime startDate, LocalDateTime endDate,SpindelEntity spindel ) {
		
		readingRepository.deleteBySpindel_IdAndDateTimeBetween(spindel.getId(), startDate, endDate);
		//TODO
		return true;
	}
	
	public boolean storeReadingReturnSuccess(IspindelPostMessage postMessage) {
		//TODO validation / sanity check
		SpindelEntity spindel = spindelService.findOrCreateSpindelByName(postMessage.getName());
		
		ReadingEntity entity = readingRepository.save(new ReadingEntity(spindel,LocalDateTime.now(),new BigDecimal(postMessage.getAngle()),new BigDecimal(postMessage.getTemperature()),new BigDecimal(postMessage.getBattery()),new BigDecimal(postMessage.getGravity())));
		
		return true;
	}
}
