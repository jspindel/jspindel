package nl.digitalalchemy.jspindel.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.digitalalchemy.jspindel.dao.BrewSessionRepository;
import nl.digitalalchemy.jspindel.dto.BrewSessionDto;
import nl.digitalalchemy.jspindel.entity.BrewSessionEntity;
import nl.digitalalchemy.jspindel.entity.SpindelEntity;

@Service
public class BrewSessionService {
	
	@Autowired
	BrewSessionRepository brewSessionRepository;
	
	@Autowired
	ReadingService readingService;
	
	@Autowired
	SpindelService spindelService;


	public boolean startSession (Long spindelId,String sessionName) {
		//TODO checks, verification .. etc
		SpindelEntity spindel = spindelService.findSpindelById(spindelId);
		if(spindel != null) {
			brewSessionRepository.save(new BrewSessionEntity(LocalDateTime.now(),spindel,sessionName));
		}
		
		return true;
	}

	public boolean stopSession (Long brewSessionEntityId) {
		//TODO checks, verification .. etc
		Optional<BrewSessionEntity> session = brewSessionRepository.findById(brewSessionEntityId);
		
		if(session.isPresent()) {
			BrewSessionEntity entity = session.get();
			entity.setEndDateTime(LocalDateTime.now());
			brewSessionRepository.save(entity);
		}

		
		return true;
	}

	public boolean deleteSession (Long brewSessionEntityId) {
		//TODO checks, verification .. etc
		Optional<BrewSessionEntity> session = brewSessionRepository.findById(brewSessionEntityId);
		
		if(session.isPresent()) {
			BrewSessionEntity entity = session.get();
			readingService.deleteReadingsReturnSuccess(entity.getStartDateTime(), entity.getEndDateTime(), entity.getSpindels().get(0));//TODO this can be  list of spindels
			entity.removeSpindel(entity.getSpindels().get(0));
			brewSessionRepository.delete(entity);
		}

		
		return true;
	}
	
	public BrewSessionEntity retrieveBrewSessionEntityById(Long id) {
		return brewSessionRepository.findById(id).get();
	}
	
	public List<BrewSessionEntity> retrieveOpenBrewSessionEntities() {
		return brewSessionRepository.findByEndDateTimeNullOrderByStartDateTimeDesc();
	}
	
	public List<BrewSessionDto> retrieveOpenSessions() {
		//what if none ?
		return this.retrieveOpenBrewSessionEntities().stream()
        .map(session -> convertBrewSessionEntityToBrewSessionDto(session))
        .collect(Collectors.toList());
	}

	public List<BrewSessionDto> retrieveClosedSessions() {
		return brewSessionRepository.findByEndDateTimeNotNull().stream()
        .map(session -> convertBrewSessionEntityToBrewSessionDto(session))
        .collect(Collectors.toList());
	}
	
	private BrewSessionDto convertBrewSessionEntityToBrewSessionDto(BrewSessionEntity entity) {
		
		BrewSessionDto dto = new BrewSessionDto();
		
		dto.setEndDateTime(entity.getEndDateTime());
		dto.setSessionName(entity.getSessionName());
		dto.setSpindelName(entity.getSpindels().get(0).getName()); //TODO this can be  list of spindels
		dto.setSpindelId(entity.getSpindels().get(0).getId());
		dto.setStartDateTime(entity.getStartDateTime());
		dto.setId(entity.getId());
		
		return dto;
	}
}
