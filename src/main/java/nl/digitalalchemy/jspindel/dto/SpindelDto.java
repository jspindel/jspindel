package nl.digitalalchemy.jspindel.dto;

public class SpindelDto {
	
	private String name;
	private boolean activeBrewSession;
	private Long id;

	
	public SpindelDto() {
		super();
		// TODO Auto-generated constructor stub
	}


	public SpindelDto(String name, Long id) {
		super();
		this.name = name;
		this.id = id;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean getActiveBrewSession() {
		return activeBrewSession;
	}

	public void setActiveBrewSession(boolean activeBrewSession) {
		this.activeBrewSession = activeBrewSession;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}
	
	

}
