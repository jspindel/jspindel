package nl.digitalalchemy.jspindel.dto;

import java.time.LocalDateTime;

public class BrewSessionDto {
	
	private LocalDateTime startDateTime;
	private LocalDateTime endDateTime;
	private String spindelName;
	private Long spindelId;
	private String sessionName;
	private Long id;
	
	public LocalDateTime getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(LocalDateTime startDateTime) {
		this.startDateTime = startDateTime;
	}
	public LocalDateTime getEndDateTime() {
		return endDateTime;
	}
	public void setEndDateTime(LocalDateTime endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getSpindelName() {
		return spindelName;
	}
	public void setSpindelName(String spindelName) {
		this.spindelName = spindelName;
	}
	public String getSessionName() {
		return sessionName;
	}
	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSpindelId() {
		return spindelId;
	}
	public void setSpindelId(Long spindelId) {
		this.spindelId = spindelId;
	}
	
	

}
