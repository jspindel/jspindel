package nl.digitalalchemy.jspindel.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ReadingDto {
	
	private Long id;
	private String spindleName;
	private LocalDateTime dateTime;
	private BigDecimal angle;
	private BigDecimal temperature;
	private BigDecimal battery;
	private BigDecimal gravity;
	
	public Long getDateTimeAsMs() {
		//TODO better solution
		ZonedDateTime zdt = dateTime.atZone(ZoneId.of("Europe/Amsterdam"));
		return zdt.toInstant().toEpochMilli();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSpindleName() {
		return spindleName;
	}

	public void setSpindleName(String spindleName) {
		this.spindleName = spindleName;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public BigDecimal getAngle() {
		return angle;
	}

	public void setAngle(BigDecimal angle) {
		this.angle = angle;
	}

	public BigDecimal getTemperature() {
		return temperature;
	}

	public void setTemperature(BigDecimal temperature) {
		this.temperature = temperature;
	}

	public BigDecimal getBattery() {
		return battery;
	}

	public void setBattery(BigDecimal battery) {
		this.battery = battery;
	}

	public BigDecimal getGravity() {
		return gravity;
	}

	public void setGravity(BigDecimal gravity) {
		this.gravity = gravity;
	}


}
