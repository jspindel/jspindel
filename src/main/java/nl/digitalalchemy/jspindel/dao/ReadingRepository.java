package nl.digitalalchemy.jspindel.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nl.digitalalchemy.jspindel.entity.ReadingEntity;

@Repository
public interface ReadingRepository extends CrudRepository<ReadingEntity, Long> {
	
	List<ReadingEntity> findFirst20ByOrderByIdAsc();
	ReadingEntity findFirstByOrderByIdDesc();
	
	List<ReadingEntity> findBySpindel_IdAndDateTimeBetweenOrderByIdAsc(Long id,LocalDateTime startTime,LocalDateTime endTime);
	
	void deleteBySpindel_IdAndDateTimeBetween(Long id,LocalDateTime startTime,LocalDateTime endTime);
}

