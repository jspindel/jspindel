package nl.digitalalchemy.jspindel.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nl.digitalalchemy.jspindel.entity.SpindelEntity;

@Repository
public interface SpindelRepository extends CrudRepository<SpindelEntity, Long> {

	Optional<SpindelEntity> findByName(String name);
}
