package nl.digitalalchemy.jspindel.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nl.digitalalchemy.jspindel.entity.BrewSessionEntity;

@Repository
public interface BrewSessionRepository extends CrudRepository<BrewSessionEntity, Long>  {
	List<BrewSessionEntity> findByEndDateTimeNullOrderByStartDateTimeDesc();
	List<BrewSessionEntity> findByEndDateTimeNotNull();
}
