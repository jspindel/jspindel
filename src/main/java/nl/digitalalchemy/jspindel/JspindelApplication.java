package nl.digitalalchemy.jspindel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JspindelApplication {

	public static void main(String[] args) {
		SpringApplication.run(JspindelApplication.class, args);
	}

}
