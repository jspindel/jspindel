package nl.digitalalchemy.jspindel.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity (name = "brewsession")
@Table(name = "brewsession") 
public class BrewSessionEntity implements Serializable {

	private static final long serialVersionUID = -1512841930121303916L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false,name = "startdatetime")
	private LocalDateTime startDateTime;
	
	@Column(nullable = true,name = "enddatetime")
	private LocalDateTime endDateTime;
	
    @ManyToMany
    @JoinTable(name = "brewsession_spindel",
    joinColumns = { @JoinColumn(name = "fk_brewsession") },
    inverseJoinColumns = { @JoinColumn(name = "fk_spindel") })
    private List<SpindelEntity> spindels = new ArrayList<SpindelEntity>();
    
	
	@Column(nullable = false,name = "sessionname")
	private String sessionName;

	public BrewSessionEntity(LocalDateTime startDateTime, SpindelEntity spindel, String sessionName) {
		super();
		this.startDateTime = startDateTime;
		this.sessionName = sessionName;
		this.addSpindel(spindel);
	}

	public BrewSessionEntity() {
		super();
	}

    public void addSpindel(SpindelEntity spindel) {
        this.spindels.add(spindel);
        spindel.getBrewsessionList().add(this);
    }

    public void removeSpindel(SpindelEntity spindel) {
        this.spindels.remove(spindel);
        spindel.getBrewsessionList().remove(this);
    }
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getStartDateTime() {
		return startDateTime;
	}

	public void setStartDateTime(LocalDateTime startDateTime) {
		this.startDateTime = startDateTime;
	}

	public LocalDateTime getEndDateTime() {
		return endDateTime;
	}

	public void setEndDateTime(LocalDateTime endDateTime) {
		this.endDateTime = endDateTime;
	}

	public String getSessionName() {
		return sessionName;
	}

	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}

	public List<SpindelEntity> getSpindels() {
		return spindels;
	}

	public void setSpindels(List<SpindelEntity> spindels) {
		this.spindels = spindels;
	}
	
	

}
