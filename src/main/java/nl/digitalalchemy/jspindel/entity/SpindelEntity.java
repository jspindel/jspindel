package nl.digitalalchemy.jspindel.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity (name = "spindel")
@Table(name = "spindel") 
public class SpindelEntity implements Serializable {

	private static final long serialVersionUID = -8991754506888632521L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false,name = "newdatetime")
	private LocalDateTime newDateTime;
	
	@Column(nullable = false,name = "name")
	private String name;
	
	@ManyToMany(mappedBy="spindels" ,fetch = FetchType.LAZY)
	private List<BrewSessionEntity> brewsessionList;

    @OneToMany(mappedBy = "spindel")
    private List<ReadingEntity> readings = new ArrayList<ReadingEntity>();

    
	public SpindelEntity() {
		super();
	}

	
	public SpindelEntity(String name) {
		super();
		this.name = name;
		this.newDateTime = LocalDateTime.now();
	}

    public void addReading(ReadingEntity reading) {
        this.readings.add(reading);
        reading.setSpindel(this);
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getNewDateTime() {
		return newDateTime;
	}

	public void setNewDateTime(LocalDateTime newDateTime) {
		this.newDateTime = newDateTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public List<BrewSessionEntity> getBrewsessionList() {
		return brewsessionList;
	}


	public void setBrewsessionList(List<BrewSessionEntity> brewsessionList) {
		this.brewsessionList = brewsessionList;
	}


	public List<ReadingEntity> getReadings() {
		return readings;
	}


	public void setReadings(List<ReadingEntity> readings) {
		this.readings = readings;
	}
	
}
