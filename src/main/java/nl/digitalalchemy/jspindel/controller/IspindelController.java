package nl.digitalalchemy.jspindel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nl.digitalalchemy.jspindel.dto.IspindelPostMessage;
import nl.digitalalchemy.jspindel.service.ReadingService;

@RestController
public class IspindelController {
	
	Logger logger = LoggerFactory.getLogger(IspindelController.class);
	
	@Autowired
	ReadingService readingService;
	
	@RequestMapping(value = "/ispindel", method = RequestMethod.POST)
	public HttpStatus ispindelPost(@RequestBody IspindelPostMessage message) {
		logger.debug(message.toString());
		
		boolean success = readingService.storeReadingReturnSuccess(message);
		
		if(success) {
			return HttpStatus.OK;
		} else {
			return HttpStatus.UNPROCESSABLE_ENTITY;//TODO what ??
		}
	}

}
