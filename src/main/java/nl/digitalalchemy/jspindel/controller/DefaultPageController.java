package nl.digitalalchemy.jspindel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import nl.digitalalchemy.jspindel.dto.ReadingDto;
import nl.digitalalchemy.jspindel.service.ReadingService;

@Controller
public class DefaultPageController {
	
	@Autowired
	ReadingService readingService;

	@RequestMapping("/")
	public String home(Model model) {
		
		ReadingDto reading = readingService.findLatestReading();
		
		if(reading != null) {
			model.addAttribute("reading", reading);
		}
		return "homepage";
	}


}
