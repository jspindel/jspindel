package nl.digitalalchemy.jspindel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GraphController {
	
	@RequestMapping("/graph")
	public String graph(Model model) {
		model.addAttribute("showTextFromJavaController","dummy text");
		return "graph";
	}

	@RequestMapping("/graph/{id}")
	public String showBrewSession(@PathVariable(value = "id") Long id,Model model) {
		model.addAttribute("brewSessionId",id);
		return "graph";
	}
	
}
