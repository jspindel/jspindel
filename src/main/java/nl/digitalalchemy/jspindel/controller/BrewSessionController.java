package nl.digitalalchemy.jspindel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import nl.digitalalchemy.jspindel.dto.BrewSessionDto;
import nl.digitalalchemy.jspindel.service.BrewSessionService;
import nl.digitalalchemy.jspindel.service.SpindelService;

@Controller
public class BrewSessionController {

	@Autowired
	BrewSessionService brewSessionService;
	
	@Autowired
	SpindelService spindelService;
	
	@GetMapping("/brewsession")
	public String brewsession(Model model) {
		this.fillModel(model);
		return "brewsession";
	}

	@PostMapping("/startsession")
	public String startSession(@ModelAttribute BrewSessionDto newsession,Model model) {

		//TODO error message
		boolean success = brewSessionService.startSession(newsession.getSpindelId(), newsession.getSessionName());

		return "redirect:/brewsession";
	}
	
	@GetMapping("/stopsession/{id}")
	public String stopSession(@PathVariable(value = "id") Long id) {

		//TODO error message
		boolean success = brewSessionService.stopSession(id);

		return "redirect:/brewsession";
	}

	@GetMapping("/deletesession/{id}")
	public String deleteSession(@PathVariable(value = "id") Long id) {

		//TODO error message
		boolean success = brewSessionService.deleteSession(id);

		return "redirect:/brewsession";
	}
	
	private void fillModel(Model model) {
		model.addAttribute("openSessions", brewSessionService.retrieveOpenSessions());
		model.addAttribute("historySessions", brewSessionService.retrieveClosedSessions());
		model.addAttribute("spindels", spindelService.retrieveListOfSpindels());
		model.addAttribute("newsession", new BrewSessionDto());		
	}
	
	
}
