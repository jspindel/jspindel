package nl.digitalalchemy.jspindel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import nl.digitalalchemy.jspindel.dto.ReadingDto;
import nl.digitalalchemy.jspindel.service.ReadingService;

@RestController
public class AjaxController {
	
	@Autowired
	ReadingService readingService;
	
	@ResponseBody
	@RequestMapping(value = "/showValues/{id}", method = RequestMethod.GET)
	public List<ReadingDto> showAll (@PathVariable(value = "id") Long id) {
		
		return readingService.retrieveReadingsForBrewSession(id);

	}

	@ResponseBody
	@RequestMapping(value = "/showValues", method = RequestMethod.GET)
	public List<ReadingDto> showLast () {
		return readingService.retrieveLastReadings();
	}

}
