package nl.digitalalchemy.ispindel;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import nl.digitalalchemy.jspindel.dao.ReadingRepository;
import nl.digitalalchemy.jspindel.entity.ReadingEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
class IspindelendpointApplicationTests {
	
	@Autowired
	ReadingRepository readingRepository;
	
	@BeforeEach
	public void init() {
		/*
		 * IspindelPostMessage [name=iSpindel000, token=BBFF-r6liHOc3nhnWjpSLLHfZi7OURjrQrj, angle=41.15717, temperature=23.1875, temp_units=C, battery=4.171011, gravity=8.34543, interval=10, RSSI=null]
		 */
//		LocalDateTime now = LocalDateTime.now();
//		BigDecimal startTemp = new BigDecimal("23.0");
//		Random rn = new Random();
//		
//	      for (int index = 1; index < 40; index++) {
//	    	  Reading reading = new Reading();
//	    	  reading.setAngle(index < 30 ? new BigDecimal(("41.157"+String.valueOf(index))).setScale(5):new BigDecimal(("40.123"+String.valueOf(index))).setScale(5));
//	    	  reading.setBattery(index < 20 ?new BigDecimal("4.171011").setScale(6):new BigDecimal("4.163011").setScale(6));
//	    	  reading.setGravity(index < 30 ?new BigDecimal("1.04001").setScale(5):new BigDecimal("1.03855").setScale(5));
//	    	  reading.setSpindleName("Spindel1");
//	    	  int randomNum = rn.nextInt((9) + 1);
//	    	  BigDecimal randomTemp = startTemp.add(new BigDecimal(randomNum*0.1)).setScale(2,RoundingMode.HALF_UP);
//	    	  
//	    	  reading.setTemperature(randomTemp);
//	    	  reading.setDateTime(now.plusMinutes(10*index));
//
//
//	    	  readingRepository.save(reading);
//	        }
	}

	@Test
	void contextLoads() {
	}

	@Test
	void retrieveReadingsTest() {
		List<ReadingEntity> readings = readingRepository.findFirst20ByOrderByIdAsc();
		for(ReadingEntity reading : readings) {
			System.out.println(reading.toString());
		}
		assertEquals(20, readings.size());
	}
}
