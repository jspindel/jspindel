for /f "delims=" %%a in (settings.ini) do set %%a

java -Dspring.profiles.active="docker" -jar ../target/Jspindel-0.1.jar --MARIADB_DB_USERNAME="%MARIADB_DB_USERNAME%" --MARIADB_DB_PASSWORD="%MARIADB_DB_PASSWORD%" --MARIADB_DB_SERVER="%MARIADB_DB_SERVER%" --MARIADB_DB_PORT=%MARIADB_DB_PORT%
